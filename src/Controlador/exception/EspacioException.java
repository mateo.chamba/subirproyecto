
package Controlador.exception;

/**
 *
 * @author MateoChamba
 */
public class EspacioException extends Exception{
    public EspacioException(String message){
        super(message);
    }
    
    public EspacioException(){
        super("No hay espacio");
    }
}
