
package Controlador.util;

import Modelo.Sucursal;
import Modelo.Ventas;

/**
 *
 * @author MateoChamba
 */
public class Utilidades {
    public static void imprimir(Object[] objs){
        System.out.println("LIsta de " + objs.getClass().getSimpleName());
        for(Object o: objs){
            System.out.println(o.toString());
        }
    }
    public static Double sumarVentas (Sucursal s){
        if (s.getVentas() != null){
            Double ventas = 0.0;
            for(Ventas v : s.getVentas()){
                ventas += v.getValormensual();
                
            }
            
        }
        return 0.0;
    }
    
    
    public static Double mediaVentas(Sucursal s){
        Double suma = sumarVentas(s);
         
        if (suma == 0 )
            return suma; 
        else 
            return suma/(s.getVentas().length);
    }
}
