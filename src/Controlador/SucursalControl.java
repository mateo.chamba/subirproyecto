
package Controlador;

import Controlador.exception.EspacioException;
import Modelo.EnumMes;
import Modelo.Sucursal;
import Modelo.Ventas;

/**
 *
 * @author MateoChamba
 */
public class SucursalControl {

    private Sucursal[] sucursales;
    private Sucursal sucursal;
    private Ventas venta;

    public SucursalControl() {
        sucursales = new Sucursal[4];
        
    }

    public Ventas getVenta() {
        return venta;
    }

    public void setVenta(Ventas venta) {
        this.venta = venta;
    }

    public Sucursal getSucursal() {
        if (sucursal == null) {
            sucursal = new Sucursal();
        }
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public Sucursal[] getSucursales() {
        return sucursales;
    }

    public void setSucursales(Sucursal[] sucursales) {
        this.sucursales = sucursales;
    }

    public boolean registrar() throws EspacioException {
        
        int pos = -1;
        int cont = -1;
        for (Sucursal s : sucursales) {
            cont++;
            if (s == null) {
                pos = cont;
                break;
            }

        }
        if (pos == -1) {
            throw new EspacioException();
        }

        sucursal.setVentas(new Ventas[EnumMes.values().length]);
         
        
        for (int i = 0; i < EnumMes.values().length; i++) {
            Ventas venta = new Ventas();
            venta.setId((i + 1));
            venta.setMes(EnumMes.values()[i]);
           
            venta.setValormensual(0.0);
            sucursal.getVentas()[i] = venta;
            
        }

        sucursales[pos] = sucursal;
        return true;

    }

    public boolean guardarVentas(Integer posVenta, Double valor) throws EspacioException{
       if (this.sucursal != null) {
           if (posVenta <= this.sucursal.getVentas().length-1)
               
           sucursal.getVentas()[posVenta].setValormensual(valor);
           
           else throw new EspacioException();
  
       }else 

           throw new NullPointerException("Debe seleccionar una sucursal");
       
     return true;
    }
    


    
}
