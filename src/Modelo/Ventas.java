
package Modelo;

/**
 *
 * @author MateoChamba
 */
public class Ventas {
    private Integer id;
    private double valormensual;
    //private Integer anio;
    private EnumMes mes;
    
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValormensual() {
        return valormensual;
    }

    public void setValormensual(double valormensual) {
        
        
        this.valormensual = valormensual;
    }

    public EnumMes getMes() {
        
        return mes;
    }

    public void setMes(EnumMes mes) {
        this.mes = mes;
    }
    
    public String toString(){
        return mes.toString()+" "+valormensual;
    }
    
}
