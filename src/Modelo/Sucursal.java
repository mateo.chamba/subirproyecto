
package Modelo;

/**
 *
 * @author MateoChamba
 */
public class Sucursal {
    private Integer id;
    private String nombre;
    private Ventas[] ventas;
    
    
    

    public Ventas[] getVentas() {
        return ventas;
    }

    public void setVentas(Ventas[] ventas) {
        this.ventas = ventas;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String toString(){
        return nombre+""+id;
    }
    
}
