
package Vista;

import Controlador.util.Utilidades;
import static Controlador.util.Utilidades.sumarVentas;

import Modelo.Ventas;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author mateochamba
 */
public class ModeloTablaVenta extends AbstractTableModel{
    private Ventas [] datos = new Ventas[12];

    /**
     * @return the datos
     */
    public Ventas[] getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(Ventas[] datos) {
        this.datos = datos;
    }
    
    @Override
    public int getColumnCount(){
        
        return 3;
        
    }

    @Override
    public int getRowCount(){
        return datos.length;
    }
   
    @Override
    public Object getValueAt(int i, int i1){
        Ventas s = datos[i];
        switch(i1){
            case 0: return (s != null) ? s.getMes().toString() : "NO DEFINIDO" ; //Operador ternario
            case 1: return (s != null) ? s.getValormensual() : 0.0;
            
            
            default:
                return null;
            
        }
    }

    @Override
    public String getColumnName(int column) {
        
         switch(column){
            case 0: 
                return "MES";
            case 1: 
                return "VALOR";
            
            default:
                return null;
    
            // Ver el nombre de las columnas
         }
    }
}