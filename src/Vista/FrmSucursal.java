
package Vista;

import Controlador.SucursalControl;
import Controlador.exception.EspacioException;
import javax.swing.JOptionPane;
import Modelo.Sucursal;
import Vista.ModeloTablaSucursal;

/**
 *
 * @author mateochamba
 */
public class FrmSucursal extends javax.swing.JDialog {

   private SucursalControl control = new SucursalControl();
   private ModeloTablaSucursal modelo = new ModeloTablaSucursal();
   
   public FrmSucursal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarTabla();
    }
   
    
    private void cargarTabla(){
        modelo.setDatos(control.getSucursales());
        tblTabla.setModel(modelo);
        tblTabla.updateUI();
        
    }
    
    public void limpiar ( ){
        
        this.control.setSucursal(null);
        txtNombre.setText("");
        cargarTabla();
    }
    
    public void guardarSucursal() {
        if (!txtNombre.getText().trim().isEmpty()) {
            this.control.getSucursal().setNombre(txtNombre.getText());

            try {
                this.control.registrar();
                limpiar();
                JOptionPane.showMessageDialog(null, "SE ha guardado","OK" ,JOptionPane.INFORMATION_MESSAGE);
                
            } catch (EspacioException ex) {
                JOptionPane.showMessageDialog(null, "Error","Error",JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Error, campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }
     
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnVentas = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Sucursal"));
        jPanel3.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jPanel2.setLayout(null);

        jLabel1.setForeground(new java.awt.Color(153, 153, 153));
        jLabel1.setText("Nombre");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(30, 70, 90, 17);
        jPanel2.add(txtNombre);
        txtNombre.setBounds(150, 70, 140, 23);

        btnGuardar.setBackground(new java.awt.Color(0, 51, 204));
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel2.add(btnGuardar);
        btnGuardar.setBounds(610, 60, 78, 23);

        jPanel3.add(jPanel2);
        jPanel2.setBounds(10, 59, 740, 140);

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel3.add(jPanel1);
        jPanel1.setBounds(10, 200, 740, 160);

        btnVentas.setText("Ventas");
        btnVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVentasActionPerformed(evt);
            }
        });
        jPanel3.add(btnVentas);
        btnVentas.setBounds(50, 380, 72, 23);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 0, 760, 430);

        setSize(new java.awt.Dimension(774, 462));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
//     SucursalControl control = new SucursalControl();
     
        try {
            control.getSucursal().setNombre(this.txtNombre.getText());
            control.registrar();
            control.setSucursal(null);
            cargarTabla();
            
            
            JOptionPane.showMessageDialog(null, "Guardado");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se puede seguir almacenando");
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVentasActionPerformed
        int fila = tblTabla.getSelectedRow();
        
        if (fila >= 0){
            if(modelo.getDatos()[fila] != null ){
            
                this.control.setSucursal(modelo.getDatos()[fila]);
                new FrmVentas(null,true,this.control).setVisible(true);
                cargarTabla();
                tblTabla.clearSelection();
            }else 
                JOptionPane.showMessageDialog(null, " Ingrese el nombre de la sucursal","Error",JOptionPane.ERROR_MESSAGE);
            
        } else 
            JOptionPane.showMessageDialog(null, " Selecciones una sucursal","Error",JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_btnVentasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmSucursal dialog = new FrmSucursal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnVentas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTabla;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
