
package pruebaunidaduno;

import Controlador.SucursalControl;
import Controlador.util.Utilidades;

/**
 *
 * @author MateoChamba
 */
public class PruebaUnidadUno {

    
    public static void main(String[] args) {
        SucursalControl sc = new SucursalControl();
        try {
            //SucursalControl sc = new SucursalControl();
            sc.getSucursal().setId(1);
            sc.getSucursal().setNombre("Mateo  ");
            sc.registrar();
            sc.guardarVentas(1, 20.0);
            sc.setSucursal(null);
            
            
            sc.getSucursal().setId(2);
            sc.getSucursal().setNombre("Luis  ");
            sc.registrar();
            sc.setSucursal(null);
            
            sc.getSucursal().setId(3);
            sc.getSucursal().setNombre("Pool  ");
            sc.registrar();
            sc.setSucursal(null);
            
            sc.getSucursal().setId(4);
            sc.getSucursal().setNombre("Cristian  ");
            sc.registrar();
            Utilidades.imprimir(sc.getSucursal().getVentas());
            
            sc.setSucursal(null);
            

            Utilidades.imprimir(sc.getSucursales());
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            Utilidades.imprimir(sc.getSucursales());
            
        }
    }
    
    
    
}
